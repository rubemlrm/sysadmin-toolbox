#!/usr/bin/perl
package gestaocron;
use warnings;
use strict;
use autodie;

#declaração de variáveis
my $min ;
my $horas ;
my $dia;
my $mes;
my $diasemana;
my $uid = `id -u`;
my $opt;
my $comando;
my $user;
my $val;
my $verifile;

#funções
sub Eliminar{
if ($uid != 0){
	&User();
	system("nano /var/spool/cron/crontabs/$user");
	system("sleep 2");
	print "Processo Concluido.A retornar para o menu inicial\n";
	system ("sleep 2");
	&GcMain();
}else{
	system("nano /etc/crontab");
	system("sleep 2");
	print "Processo Concluido.A retornar para o menu inicial\n";
	system ("sleep 2");
	&GcMain();
}
}

sub Adicionar{
&Minutos();
&Horas();
&Dia();
&Mes();
&DiaSemana();
&Comando();
&Final();
}

sub Minutos{
print "Introduza os parametros de agendamento\n";
print "Introduza os minutos\n";
print ("Se desejar que o script seja executado em todos os minutos - pressione a\n");
print ("Se desejar especificar o minuto - pressione c \n");
chomp($opt = <STDIN>);
	if ($opt eq "a"){
		$min = "*";
	}elsif($opt eq "c"){
	 &Validar();
	 $min=$val;
	}else{
		print "opção inválida\n";
		system("clear");
		&Minutos();
	}
}
1;

sub Horas{
print "Introduza as horas\n";
print ("Se desejar que o script seja executado em todos as horas - pressione a\n");
print("Se desejar especificar as horas - pressione c \n")	;
chomp($opt = <STDIN>);
	if ($opt eq "a"){
		$horas = "*";
	}elsif($opt eq "c"){
	 &Validar();
	 $horas = $val;
	}else{
		system("clear");
		print "opção inválida\n";
		&Horas;
	}
}


sub Dia{
print "Introduza os dia\n";
print ("Se desejar que o script seja executado em todos os dias - pressione a\n");
print ("Se desejar especificar o dia - pressione c \n")	;
chomp($opt = <STDIN>);
	if ($opt eq "a"){
		$dia = "*";
	}elsif($opt eq "c"){
	 &Validar();
	 $dia = $val;
	}else{
		print "opção inválida\n";
		system("clear");
		&Dia();
	}

}

sub Mes{
print "mes\n";
print ("Se desejar que o script seja executado todos os meses - pressione a\n");
print (" Se desejar especificar o mes - pressione c \n")	;
chomp($opt = <STDIN>);
	if ($opt eq "a"){
		$mes = "*";
	}elsif($opt eq "c"){
	 &Validar();
	 $mes= $val;
	}else{
		print "opção inválida\n";
		system("clear");
		&Mes();
	}	
}

sub DiaSemana{
print "Dia da Seman\n";
print("Se desejar que o script seja executado todos os diass - pressione a\n");
print("Se desejar especificar o dia - pressione c \n")	;
chomp($opt = <STDIN>);
	if ($opt eq "a"){
		$diasemana="*";
	}elsif($opt eq "c"){
	 &Validar();
	 $diasemana = $val;
	}else{
		print "opção inválida\n";
		system("clear");
		&DiaSemana();
		
	}
}

sub Comando(){
print  ("Introduza o comando ou script a executar\n");

chomp(my $script = <STDIN>);
$comando = $script;
}


sub User(){
	chomp($user = `whoami`);
}

sub ChUser(){
	print "Escolha o utilizador que vai executar o script/comando\n";
	chomp($user = <STDIN>);
}


#funcoes de validação
sub Validar(){
	print "Introduza o valor\n";
	chomp($val = <STDIN>);
	if ($val =~ /d\,?\/?-?/){
	print "Valor guardado\n";
	system("clear");
	}else{
	&ErrorVal();
	}
}

sub ErrorVal(){
	print "Valor inválido\n";
	print "Deseja sair ? se sim pressione S\n";
	print "Senão, se quiser voltar a repetir este passo pressione R";
	chomp($opt= <STDIN>);
	if($opt eq "S"){
		exit;	
	}elsif($opt eq "R"){
		system("clear");
		&Validar();
		
	}else{
		print "Opção errada\n";
		&ErrorVal();
	}
}
sub VeriFile(){
if($uid != 0){
	&User;
	if(-e "/var/spool/cron/crontabs/$user"){
		$verifile = ">>/var/spool/cron/crontabs/$user"
	}else{
		$verifile = ">/var/spool/cron/crontabs/$user"
	}
}else{
  if(-e "/etc/crontab"){
  	$verifile = ">>/etc/crontab"
}else{
	$verifile = ">/etc/crontab"
		}
	}
}

sub Final(){
if ($uid != 0){
	&VeriFile();
	open(FILE, "$verifile");
	print FILE "$min $horas $mes $dia $diasemana $comando\n";
	close(FILE);
}else{
	&ChUser();
	open(FILE, "$verifile");
	print FILE "$min $horas $mes $dia $diasemana $user $comando\n";
    close(FILE);
	}
}
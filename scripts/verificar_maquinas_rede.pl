#!/usr/bin/perl
package networking;
use warnings;
use strict;
use Net::Ping;
use autodie;
my $host;
my $my_addr="<ip>";
my $ping;	
my @reportlist;
chomp(my $date = `date +%a%d%m%y%k%M`);

sub VerificarMaquinasRede(){
open(FILE, "../files/listagem_ip_rede.txt");
chomp (my @ips = <FILE>);
close(FILE);
 
foreach $host (@ips){
    $ping = Net::Ping->new();
    if ($ping->ping($host)){
    #print "$host -> está activo.\n"; 
    push(@reportlist,"$host -> está activo.");
    }else{    	
    #print "$host -> está offline\n";
    push(@reportlist,"$host -> está offline.");
    }
    $ping ->close();
}

#Reporte da lista
foreach (@reportlist) 
{
    print "$_\n";
}
	
#para um ficheiro a ser enviado por mail

	open(FILE , ">../logs/$date\_machine_networking_report.txt");
    foreach (@reportlist){		
    print FILE "$_\n";
	}
    close(FILE);
}
1;
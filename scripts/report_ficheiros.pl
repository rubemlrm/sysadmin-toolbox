#!/usr/bin/perl
package FindFiles;
use warnings;
use strict;
use File::Find;
#Vars
my %size;

#funções a serem chamadas



#
sub DiskMain(){
my $size;
my $path;
my $tempvar;
print "Bem vendo ao gestor de Disco\n";
print "Se deseja encontrar ficheiros com mais do que X numa determinada directoria\n";
print "Escolha F\n";
print "Defina o tamanho em MB\n";
chomp($tempvar = <STDIN>);
$size= "$tempvar";
print "Defina a directoria\n";
chomp($path = <STDIN>);

FindFiles($size, $path);

}

sub FindFiles($$){
	my $size = $_[0];
	my $path = $_[1];

find(sub {$size{$File::Find::name} = -s if -f;}, $path);}


sub Listagem(){
chomp(my $date = `date +%a%d%m%y%k%M`);

my @sorted;

@sorted = sort {$size{$b} <=> $size{$a}} keys %size;
    
splice @sorted, 20 if @sorted > 20;


#Para o ecrã;   
foreach (@sorted) 
{
    printf "%10d %s\n", $size{$_}, $_;
}
	
#para um ficheiro a ser enviado por mail
foreach (@sorted) 
{
	open(FILE , ">>../logs/$date\_file_reporting_size.txt");
    printf FILE "%10d %s\n", $size{$_}, $_;
    close(FILE);
}
SendMail($date);
}	

sub SendMail($){
	my $date = $_[0];
	
    my $dest="rubemmota89\@gmail.com";
    my $ass ="Report $date";
    
    system("cat ../logs/$date\_log.txt | mail -s $ass $dest");
}
1;
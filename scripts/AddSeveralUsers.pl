#!/usr/bin/perl
package GesSevUsers;
use strict;
use warnings;


sub MassUserAdd(){
my $pass;
my $endsalt = `date +%d%m%y`;	
open (FILE, "../files/listagem_users.txt") || die($!);
my @accList = <FILE>;
close(FILE);


foreach (@accList){
   chomp($_);
   $pass = crypt($_, $endsalt);
   system("useradd -p '$pass' -m $_");
   
	if ($? == 0)   #  $? Devolve o ”código de erro” da última chamada ao sistema
	{
  	 print("Conta $_ criada com sucesso\n")
	}
	else
	{
  	 print("Erro ao tentar criar a conta $_ \n")
	}
	system("chage -M 30 -W 10 $_");#define que a pass apenas é válida durante 30 dias , e 10 dias antes começa a avisar os utilizadores que tem que mudar a pass
	}
}
1;
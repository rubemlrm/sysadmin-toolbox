#!/usr/bin/perl
#alpha version
package gestaousers;
use warnings;
use strict;

#vars
my $opt;
my $user;
my $folder;

#funções a serem chamadas

sub Remover(){
	print "Escolha qual é a opção que quer usar";
	print "1-Se deseja apenas apagar o utilizador e deixar a pasta home e o seu grupo";
	print "2-Se desejar apagar o registo de utilizador e a sua pasta home";
	print "3-Se desejar apagar tudo o que seja relativo ao utilizador";
}

sub Opt2{
chomp($opt = <STDIN>);
if ( $opt eq "1"){
	&DelUser();	
}elsif($opt eq "2"){
	&DelUserHome();
}elsif($opt eq "3"){
	&DelAllStuff();
}elsif($opt eq "4"){
	system("./\../menu.pl");
}else{
	print "opção errada";
	&Opt2;
}
}

sub DelUser(){
	print "Introduza o nome do utilizador que vai ser apagado";
	chomp ($user = <STDIN>);
	system ("deluser $user");
	&ValCmd();
	
}
sub DelUserHome(){
	print "Introduza o nome do utilizador que vai ser apagado";
	chomp ($user = <STDIN>);
	&BackupHome();
	system("deluser --remove-home $user");
	&ValCmd();

}

sub ValCmd(){
	if ($? == 0)   
	{
  	 print("Operação efectuada com sucesso\n")
	}
	else
	{
  	 print("Falhou\n")
	}
}

sub ValFolder(){
	if (-d $folder){
		print "Verificação da pasta com sucesso";
		print "A iniciar backup\n";
		
	}else{
		print "Pasta de backup não existe\n";
		print "A efectuar criação da pasta";
		system("mkdir -p $folder");
		&ValCmd();
		print "A iniciar Backup\n";
	}
	
}
sub BackupHome(){
	print "Visto que esta operação vai apagar todos os ficheiros que estão na pasta home deste utilizador\n";
	print "Deseja efectuar o backup dos mesmos?(S\\N)\n";
	chomp($opt = <STDIN>);
	if ($opt eq "S"){
		print "especifique a pasta de destino do backup\n";
		chomp($folder = <STDIN>);
		&ValFolder;
		system("mv -R /home/$user $folder");
		print "BackUp concluido\n";
	}else{
		print "a iniciar processo de eliminação";
	}
}

sub BackupAll(){
	print"Será efectuado um backup de todos os ficheiros deste utilizador\n";
	print"Deseja efectuar o bados dos mesmos?(S\\N)\n";
	chomp($opt = <STDIN>);
	if($opt eq "S"){
		print "especifique a pasta de destino do backup\n";
		chomp($folder = <STDIN>);
		&ValFolder;
		open(FILE, "find ./ -user $user") || die($!);
		chomp(my @ffile = (<FILE>));
		close(FILE);
		foreach(@ffile){
			if($_ == -d){
			system("mv -R $_ $folder");
		}else{
			system("cp -R $_ $folder");
		}
	}
	}else{
		print "O backup não será efectuado";
	}
	
}

sub DelAllStuff(){
	print "Visto que esta operação vai apagar todos os ficheiros  deste utilizador\n";
	print "Deseja efectuar o backup dos mesmos?(S\\N)\n";
	print "É  recomendado visto que este utilizador pode ter ficheiros noutras contas\n";
	chomp($opt = <STDIN>);
	if ($opt eq "S"){
		print "especifique a pasta de destino do backup\n";
		chomp($folder = <STDIN>);
		&ValFolder;
		open(FILE, "find ./ -user $user") || die($!);
		chomp(my @ffile = (<FILE>));
		close(FILE);
		foreach(@ffile){
			if($_ == -d){
			system("mv -R $_ $user");
		}else{
			system("cp -R $_ $user");
		}
	}
		
		
		
		print "BackUp concluido\n";
	}else{
		print "a iniciar processo de eliminação";
	}
}

#funções para adicionar utilizadores

sub AddUser(){

print("Introduza o nome de utilizador que deseja introduzir\n");
chmop(my $user = <STDIN>);
system("adduser $user");
}

sub Verifify_User {
	#Variaveis a serem utilizadas
	my(@finalusers, $utilizador,$uid, $guid, $folder);
	my $statuscode = 0;

	print "Introduza o nome de utilizador , sobre quem deseja receber as informações \n";
	chomp($utilizador = <STDIN>);

	open(FILE, "/etc/passwd") || die($!);
	chomp(my @utilizadores = <FILE>);
	close(FILE);

	foreach my $date(@utilizadores){
		chomp $date;
		my @finalusers = split /:/, $date;
		chomp(@finalusers);
		if($finalusers[0] eq $utilizador){
			$uid = $finalusers[2];
			$guid = $finalusers[3];
			$folder = $finalusers[5];
			$statuscode++;

		}
	}
	if($statuscode != 1){
		print "O $utilizador não existe no sistema \n";
	}else{
		print "O $utilizador já se encontra criado no sistema com as seguintes informações\n";
		print "UID : $uid\n";
		print "GUID : $guid\n";
		print "Pasta home: $folder\n";
	}

}

sub List_Users(){
	my(@utilizadores, $uid, $guid, $folder,@groups);
	open(FILE, "/etc/passwd" || die($!));
	chomp(@utilizadores = <FILE>);
	close(FILE);
	
	foreach my $date (@utilizadores){
		chomp $date;
		my @finalusers = split /:/, $date;
		if($finalusers[2] >= 1000 && $finalusers[2] < 10000){
		$uid = $finalusers[2];
		$guid = $finalusers[3];
		$folder = $finalusers[5];
		print"NOME: $finalusers[0]\n";
		print"UID:$uid \n";
		print"GUID:$guid \n";
		print"HOME:$folder \n";
		}
	}	
}

sub Remove_User_FGroup(){
	my ($user, $grupo);
	print ("Introduza o grupo do qual o utilizador será removido");
	chomp($grupo = <STDIN>);
	print"Introduza o nome do utilizador que quer remover de um $grupo";
	chomp($grupo = <STDIN>);
	system("gpasswd -d $user $grupo");
}
sub Add_user_TGroup(){
	my ($user, $grupo);
	print ("Introduza o grupo do qual o utilizador será removido");
	chomp($grupo = <STDIN>);
	print"Introduza o nome do utilizador que quer remover de um $grupo";
	chomp($grupo = <STDIN>);
	system("gpasswd -a $user $grupo");
}
sub MassUserAdd(){
my $pass;
my $endsalt = `date +%d%m%y`;	
open (FILE, "../files/listagem_users.txt") || die($!);
my @accList = <FILE>;
close(FILE);


foreach (@accList){
   chomp($_);
   $pass = crypt($_, $endsalt);
   system("useradd -p '$pass' -m $_");
   
	if ($? == 0)   #  $? Devolve o ”código de erro” da última chamada ao sistema
	{
  	 print("Conta $_ criada com sucesso\n")
	}
	else
	{
  	 print("Erro ao tentar criar a conta $_ \n")
	}
	system("chage -M 30 -W 10 $_");#define que a pass apenas é válida durante 30 dias , e 10 dias antes começa a avisar os utilizadores que tem que mudar a pass
	}
}

1;
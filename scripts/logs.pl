#!/usr/bin/perl
package Logs;
use warnings;
use strict;

sub Logs(){
#local vars
chomp(my $date = `date +%a%d%m%y%k%M`);
my $uid = `id -u`;
my $cmd;
my @log;
my @array =("/var/log/cron.log", "/var/log/mail.log", "/var/log/messages.log", "/var/log/secure.log");

if ($uid != 0){
	print "Impossivel executar este script.É necessário privilégios root\n";
}else{
foreach(@array){
	open(FILE, "$_");
	my @temlog = <FILE>;
	close(FILE);
	@log = @temlog;
	
}
}
open(FILE, ">../logs/$date\_/systemlogs.txt") || die($!);
print FILE ("@log\n");
close(FILE);
}
1;
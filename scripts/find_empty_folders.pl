#!/usr/bin/perl 
use strict; 
use warnings; 
package EmptyFolders;
use File::Find::Rule; 


sub FindEmptyFolders(){

my @sub_dirs;
print "Introduza o caminho que deseja verifcar\n";
chomp(my $path = <STDIN>);

#Listagem de todos os directórios
my $regra_pesquisa = File::Find::Rule->new;  
$regra_pesquisa->directory; 
@sub_dirs = $regra_pesquisa->in($path); 

&AnalisarDirs(@sub_dirs);
}

sub AnalisarDirs($){
my @sub_dirs= @_ ;	
my @emptydirs;

my $file2;

foreach $file2 (@sub_dirs){
       		opendir(FILE, "$file2") or die($!);
        	readdir FILE;
        	readdir FILE;
		#readdir FILE;
        	if(!(readdir FILE)){
        		print "$file2 :  directório vazio\n";
        		push(@emptydirs, $file2);
        	}
        closedir(FILE) or die "$!";

	}
	
	########Totalizaçao dos directorios###########
	my $total = @sub_dirs;
	my $vazio = @emptydirs;
	print "Total directórios" . $total .", directórios vazios " . $vazio . "\n";
	
}
1;
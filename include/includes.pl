#!/usr/bin/perl
package teste;
use strict;
use warnings;
require './scripts/logs.pl';
require './scripts/AddSeveralUsers.pl';
require './scripts/gestaocron.pl';
require './scripts/gesprocessos.pl';
require './scripts/report_ficheiros.pl';
require './scripts/verificar_maquinas_rede.pl';
require './scripts/find_empty_folders.pl';
require './scripts/gestaousers.pl';